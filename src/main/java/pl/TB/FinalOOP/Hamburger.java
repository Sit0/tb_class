package pl.TB.FinalOOP;
class Meat {
    String name;

    public Meat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

class Bread {
    private String name;

    public Bread(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

class Addition{
    private String name;
    private double price;

    public Addition(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}

public class Hamburger {

    private String name;
    private Meat meat;
    private Bread breadType;
    private Addition additiontype;
    private double price;


    public Hamburger(String name, Meat meat, Bread breadType, double price) {
        this.name = name;
        this.meat = meat;
        this.breadType = breadType;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Meat getMeat() {
        return meat;
    }

    public Bread getBreadType() {
        return breadType;
    }

    public Addition getAdditiontype() {
        return additiontype;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Hamburger" +
                "name='" + name + '\'' +
                ", meat=" + meat.getName() +
                ", breadType=" + breadType.getName() +
                ", price=" + price;
    }
}
